<?php
/**
 * WordPress theme supports
 *
 * @package GOGOmedia
 */

add_action(
	'after_setup_theme',
	function () {
		add_theme_support( 'post-thumbnails' );
	}
);

add_action(
	'init',
	function () {
		register_nav_menu( 'nav_main', __( 'Menu: main' ) );
		register_nav_menu( 'nav_top_contact', __( 'Menu: top contact' ) );
		register_nav_menu( 'nav_top_lng', __( 'Menu: top lng' ) );
		register_nav_menu( 'nav_footer', __( 'Menu: footer menu' ) );
	}
);

add_theme_support( 'admin-bar', array( 'callback' => '__return_false' ) );

add_action(
	'init',
	function () {
		if ( empty( $_GET['post'] )
		) {
			return;
		}
		$post = sanitize_text_field( wp_unslash( $_GET['post'] ) );

		if ( isset( $post ) ) {
			$template   = get_post_meta( $post, '_wp_page_template', true );
			$front_page = get_option( 'page_on_front' );
			if ( ( 'default' === $template ) && ( $front_page !== $post ) ) {
				return;
			} else {
				remove_post_type_support( 'page', 'editor' );
			}
		}
	}
);

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page(
		array(
			'page_title' => 'Theme Settings',
			'menu_title' => 'Theme Settings',
			'menu_slug'  => 'theme-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		)
	);
}

add_action(
	'init',
	function() {
		global $wp_rewrite;
		$author_slug             = 'autor';
		$wp_rewrite->author_base = $author_slug;
		$wp_rewrite->flush_rules();
	}
);

add_theme_support( 'custom-logo', array() );

add_filter( 'timber_context', 'speed_timber_context'  );

function speed_timber_context( $context ) {
	$context['options'] = get_fields('option');
	return $context;
}
