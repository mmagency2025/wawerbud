const $ = require('jquery');

const headerEl = $(".header");
const hamburgerEl = $(".js-header__hamburger");
const bodyEl = $("body");
let previousScroll = 0;

function header() {

  if (headerEl.length > 0) {
    $(window).on("scroll", function() {
      onScroll();
    })
    onScroll();
  }

  hamburgerEl.on("click", function() {
    headerEl.toggleClass("header--open");
    bodyEl.toggleClass("has-nav");
  });

}

function onScroll() {

  let currentScroll = $(window).scrollTop();

  console.log(currentScroll);

  if (currentScroll > 200){
    headerEl.addClass('header--scroll');
    if (currentScroll > previousScroll) {
      headerEl.removeClass("header--scroll-up");
      headerEl.addClass("header--scroll-down");
    } else {
      headerEl.removeClass("header--scroll-down");
      headerEl.addClass("header--scroll-up");
    }
  } else {
    headerEl.removeClass('header--scroll')
    headerEl.removeClass("header--scroll-up");
    headerEl.removeClass("header--scroll-down");
  }
  previousScroll = currentScroll;
}



export default header;
